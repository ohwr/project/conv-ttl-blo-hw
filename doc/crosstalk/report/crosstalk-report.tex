%==============================================================================
% Document header
%==============================================================================
\documentclass[a4paper,11pt]{article}

% Color package
\usepackage[usenames,dvipsnames,table]{xcolor}

% Hyperrefs
\usepackage[
  colorlinks = true,
  linkcolor  = Mahogany,
  citecolor  = Mahogany,
  urlcolor   = blue,
]{hyperref}

% Longtable
\usepackage{longtable}

% Graphics, multirow
\usepackage{graphicx}
\usepackage{multirow}

% Appendix package
\usepackage[toc,page]{appendix}

\usepackage{fancyhdr}
\setlength{\headheight}{15.2pt}
\pagestyle{fancy}
\fancyhead[L]{\nouppercase{\leftmark}}
\fancyhead[R]{}
\renewcommand{\footrulewidth}{0.4pt}

% Row number command
\newcounter{rownr}
\newcommand{\rownumber}{\stepcounter{rownr}\arabic{rownr}}

%==============================================================================
% Start of document
%==============================================================================
\begin{document}

%------------------------------------------------------------------------------
% Title
%------------------------------------------------------------------------------
\include{cern-title}

%------------------------------------------------------------------------------
% Revision history
%------------------------------------------------------------------------------
\thispagestyle{empty}
\section*{Revision history}

\centerline
{
  \rowcolors{2}{white}{gray!25}
  \begin{tabular}{l c p{.6\textwidth}}
  \hline
  \multicolumn{1}{c}{\textbf{Date}} & \multicolumn{1}{c}{\textbf{Version}} & \multicolumn{1}{c}{\textbf{Change}} \\
  \hline
  11-06-2014 & 0.1 & First draft \\
  27-06-2014 & 0.2 & Second draft, revised wording and CM current path \\
  11-07-2014 & 1   & First release of the document \\
  \hline
  \end{tabular}
}

\pagebreak
\pagenumbering{roman}
\setcounter{page}{1}
\tableofcontents

%------------------------------------------------------------------------------
% List of figs, tables
%------------------------------------------------------------------------------
\listoffigures
%\listoftables

%------------------------------------------------------------------------------
% List of abbreviations
%------------------------------------------------------------------------------
\pagebreak
\section*{List of Abbreviations}
\begin{tabular}{l l}
CM & Common-Mode \\
DM & Differential-Mode \\
\end{tabular}

\pagebreak
\thispagestyle{empty}
\mbox{}

\pagebreak
\pagenumbering{arabic}
\setcounter{page}{1}


%==============================================================================
% SEC: Intro
%==============================================================================
\section{Introduction}
\label{sec:intro}

This document presents the crosstalk issue~\cite{crosstalk-issue} on CONV-TTL-BLO systems~\cite{ctb-proj}.
The CONV-TTL-BLO system is a six-channel pulse repetition system that can replicate
3.3~V TTL pulses to 24~V so-called "blocking" pulses. TTL and blocking pulses arrive
and are sent via coaxial cables connected through LEMO 00 connectors.

As shall be shown, what we called crosstalk is actually common-impedance coupling~\cite{joffe_grounds_2010}
due to conversion of common-mode (CM) currents into differential-mode (DM) voltage.
The TTL pulse generation side is clean related to interference, but it has been observed that the blocking
output stage creates interference on other blocking output channels.

This document will begin by presenting the blocking output stage in Section~\ref{sec:blo-outp-stage},
then show the interference signal in Section~\ref{sec:signal}. The explanation
for why the problem appears is given in Section~\ref{sec:reason} and a SPICE model
to validate the explanation is given in Section~\ref{sec:spice-mdl}. The document will
end with proposed solutions described in Section~\ref{sec:solution}.

%==============================================================================
% SEC: Blo outp. stage
%==============================================================================
\section{Blocking output stage}
\label{sec:blo-outp-stage}

Figure~\ref{fig:blo-outp-stage} presents the CONV-TTL-BLO blocking output stage.
A transformer with a 1:1 coupling ratio is controlled by a MOSFET (which is controlled
by the on-board FPGA) to generate a pulse with a width of approx. 1.2~$\mu$s and a rising
edge of approx. 140~ns. The transformer is meant to provide galvanic isolation in
situations where the cables are going out of a building and into another.

\begin{figure}[h]
  \centerline{\includegraphics[width=\textwidth]{fig/blo-outp-stage}}
  \caption{\label{fig:blo-outp-stage} Blocking output stage}
\end{figure}


%==============================================================================
% SEC: Interference signal
%==============================================================================
\pagebreak
\section{Interference signal}
\label{sec:signal}

The interference appears when the output cables of two normally isolated channels have
their shields connected together. A situation such as this can be replicated for visualization by
connecting two CONV-TTL-BLO channels to a non-isolated oscilloscope (one which has
the outer conductors of the BNC connectors of all oscilloscope channels connected together).

Figure~\ref{fig:non-isolated} shows such a situation. Here, the CONV-TTL-BLO CH1 is
used to generate a signal, and CH2 is passive, but interference is generated on it.

\begin{figure}[h]
  \centerline{\includegraphics[width=.65\textwidth]{fig/non-isolated}}
  \caption{\label{fig:non-isolated} Interference when the return paths of two blocking channels are connected together}
\end{figure}

Since it is very common on CERN boards to connect the shields of coaxial cables to the board's
"ground" network (we do it on the CONV-TTL-BLO as well), a problem such as this would appear
any time two separate channels of the same CONV-TTL-BLO system would be connected to any
one remote board.

Furthermore, as can be seen in Figure~\ref{fig:non-isolated-diff-room}, the interference appears
even when two different CONV-TTL-BLO systems are placed in two crates in different rooms
and have the output signal cables connected to a non-isolated oscilloscope.

\begin{figure}[h]
  \centerline{\includegraphics[width=.68\textwidth]{fig/non-isolated-diff-room}}
  \label{fig:non-isolated-diff-room}
  \caption{Interference when the return paths of two boards in different rooms are connected together}
\end{figure}

%==============================================================================
% SEC: Reason of occurrence 
%==============================================================================
\section{Reason of occurrence}
\label{sec:reason}

The interference signal is due to the inter-winding capacitance of the selected
transformer and the fact that the coaxial cable through which the blocking signal is
transmitted is unbalanced~\cite{joffe_grounds_2010}.

Transformers in which the primary and secondary are closely spaced have increased parasitic
inter-winding capacitance, due to the proximity between the two current-carrying wires. This
capacitance is sketched in Figure~\ref{fig:transformer-parasitic-cap}.

\begin{figure}[h]
  \centerline{\includegraphics[width=\textwidth]{fig/transformer-parasitic-cap}}
  \caption{\label{fig:transformer-parasitic-cap} Transformer parasitic inter-winding capacitance}
\end{figure}

The MSD1278-104ML transformer~\cite{coilcraft_msd1278_2013} on the CONV-TTL-BLO has an inter-winding capacitance of
$\sim$220~pF (see Appendix~\ref{app:cap-meas} for details on how to measure this capacitance). This inter-winding
capacitance can be modeled as two lumped capacitors between the ports of the transformer, since the equivalent
capacitance of two capacitors in parallel is equal to the sum of their capacitances. Introducing these
lumped inter-winding capacitors into the blocking output stage of Figure~\ref{fig:blo-outp-stage} yields
Figure~\ref{fig:blo-outp-stage-w-caps}.

\begin{figure}
  \centerline{\includegraphics[width=\textwidth]{fig/blo-outp-stage-w-caps}}
  \caption{\label{fig:blo-outp-stage-w-caps} Blocking output stage with transformer inter-winding capacitors}
\end{figure}

When the MOSFET transistor switches, due to the 27~V blocking level supply, a high $dV/dt$
(27~V in about 140~ns) is created across this capacitance and this $dV/dt$ creates a current through
this capacitance:

\begin{equation}
I = C\frac{dV}{dt}
\end{equation}

This current will pass to the secondary as CM current which gets divided between the signal and return paths
and travel in phase on both connections. Since currents travel in loops, this CM current will return to its
source one way or another. Due to the ground connection of the shield at the far end of the cable, the line
is unbalanced with respect to ground, which means unequal currents will travel through the signal and return wires.
Since the voltage at the far end is measured directly with respect to ground, a CM current passing through the
50~$\Omega$ termination resistor will generate a DM interference voltage.

The CM current can be measured by placing a current probe around the coaxial cable, as shown in
Figure~\ref{fig:cm-meas-no-second-chan}. In this figure, oscilloscope channel 2 shows
the blocking signal waveform, which is used as a trigger. Oscilloscope channel 1 shows the CM
current through the coaxial cable, measured using a probe with a transfer function of 10mA/mV.
Oscilloscope channel 4 shows the CM current return through the 220~V power line of the ELMA crate
containing the CONV-TTL-BLO system. It can clearly be seen that all of the CM current through the
affecting channel goes back through the power line to the source in the parasitic capacitors of the
transformer.

Further note in Figure~\ref{fig:cm-meas-no-second-chan} the coaxial cable modeled as inductors and
some parasitic capacitance between the wires, as the cable's inductance and capacitance have an
influence in the generation of the interference voltage.

\begin{figure}
  \centerline{\includegraphics[width=.87\textwidth]{fig/cm-meas-no-second-chan}}
  \caption{\label{fig:cm-meas-no-second-chan} Common-mode current measurement}  
\end{figure}

\begin{figure}
  \centerline{\includegraphics[width=\textwidth]{fig/cm-crrt-no-second-chan-tran-off}}
  \caption{\label{fig:cm-crrt-no-second-chan-tran-off} Common-mode current to charge the parasitic capacitors when the MOSFET turns off}

  \centerline{\includegraphics[width=\textwidth]{fig/cm-crrt-no-second-chan-tran-on}}
  \caption{\label{fig:cm-crrt-no-second-chan-tran-on} Common-mode current to discharge the parasitic capacitors when the MOSFET turns on}
\end{figure}

Figures~\ref{fig:cm-crrt-no-second-chan-tran-off} and~\ref{fig:cm-crrt-no-second-chan-tran-on} show
how a current is generated through the transformer's parasitic capacitance when only the offending channel
is connected to the oscilloscope. Let us start with the current on the falling edge of the pulse, as shown
in Figure~\ref{fig:cm-crrt-no-second-chan-tran-off}. The falling edge of the pulse corresponds to a rising
edge on the transformer primary connection, due to the fact that the transistor turns off, and the voltages
at the upper and lower points of the transformer primary go from ground to 27~V. Because of this rising edge
on the primary, a positive charging current will be created from the 27~V supply rail, through the parasitic capacitance
in the transformer and will close off to its generation point at the 27~V supply through the ground connection.
This charging current can be seen on the falling edge of the output pulse in
Figure~\ref{fig:cm-meas-no-second-chan}.

On the next rising edge of the pulse, corresponding to a falling edge at the transformer primary (due to the transistor
turning on and creating a connection to ground), the charge stored in the capacitor on the previous pulse falling edge
is discharged, creating a CM current as shown in Figure~\ref{fig:cm-crrt-no-second-chan-tran-on}. This discharge
current goes towards ground and returns to its generation point at the transformer parasitic capacitors, having
an opposite sense to that in which the current probe is mounted, as can be seen in
Figure~\ref{fig:cm-meas-no-second-chan}.

A zoom-in on channel 2 (Figure~\ref{fig:cm-meas-no-second-chan-zoom}) shows a DM voltage generated as part of
the CM current flows through the signal conductor and the 50~$\Omega$ resistor to recombine with the other part
flowing through the shield.

\begin{figure}
  \centerline{\includegraphics[width=\textwidth]{fig/cm-meas-no-second-chan-zoom}}
  \caption{\label{fig:cm-meas-no-second-chan-zoom} Common-mode current generates differential-mode voltage on offending channel}
\end{figure}

When the shield of a second (victim) output on the CONV-TTL-BLO is connected to the
shield of the first channel, similar CM currents will be generated on the pulse rising and falling edges, in order
to charge and discharge the transformer parasitic capacitors, this time of both victim and offending channels.
Part of the CM current finds a low-impedance path through the shield of the victim channel, effectively splitting
between this shield conductor, the safety ground conductor of the 220~V power line and the signal conductor through
the victim channel. The CM current passing through the 50~$\Omega$ termination and the signal conductor at the
victim channel create the DM interference voltage. The current will then close off to ground through the
decoupling capacitor at the 27~V rail.

Figures~\ref{fig:cm-crrt-second-chan-tran-off} and~\ref{fig:cm-crrt-second-chan-tran-on} show how the CM current distributes
to charge and respectively discharge the parasitic capacitors in the transformer on the rising and falling edges of the pulse output
on the offending channel.

As can be seen, when the MOSFET turns off (falling edge of the pulse output, Figure~\ref{fig:cm-crrt-second-chan-tran-off}), the charging current
finds a path to ground and back to the 27~V generation point through both the safety ground conductor of the power line and the
shield of the victim channel. When it reaches the 50~$\Omega$ termination at the victim channel, part of the CM current splits also through
the signal conductor, generating a DM voltage. The current returns to ground through the parasitic capacitors of the victim channel
transformer and the decoupling capacitor at the 27~V supply rail, charging the parasitic capacitors of the victim channel
in the process.

When the MOSFET next turns on (Figure~\ref{fig:cm-crrt-second-chan-tran-on}), a falling edge on the primary of the offending
channel transformer discharges both the offending and victim channel parasitic capacitors, and a discharge current is generated
in the opposite sense.

\begin{figure}
  \centerline{\includegraphics[width=.67\textwidth]{fig/cm-crrt-second-chan-tran-off}}
  \caption{\label{fig:cm-crrt-second-chan-tran-off} Common-mode current through victim channel when offending channel MOSFET turns off}
  
  \centerline{\includegraphics[width=.67\textwidth]{fig/cm-crrt-second-chan-tran-on}}
  \caption{\label{fig:cm-crrt-second-chan-tran-on} Common-mode current through victim channel when offending channel MOSFET turns on}
\end{figure}

Figure~\ref{fig:currents-plot} shows a combined plot of current measurements on the various conductors that
carry the CM currents and how these currents were measured, in their expected sense of travel. That is, a CM
current entering the oscilloscope via the offending channel cable is expected to exit the oscilloscope through
the victim channel cable and the 220~V power cable. The combined plot shown in the figure shows that part of the
current goes through the signal conductor, even though in each of the cases in
Figures~\ref{fig:cm-crrt-second-chan-tran-off}~and~\ref{fig:cm-crrt-second-chan-tran-on} there is a diode that
can potentially be reverse-biased. This can be explained by the fact that the impedance of 1~k$\Omega$ resistor may be
reduced by parasitic capacitance in the cable and more of the current goes through the signal cable than 
would initially be expected. Also, the diode may conduct current while it is still in reverse recovery mode.

\begin{figure}
  \centerline{\includegraphics[width=.9\textwidth]{fig/currents-plot}}
  \caption{\label{fig:currents-plot} Plot of coaxial cable currents}
\end{figure}

Finally, Figure~\ref{fig:cm-meas-second-chan} shows on oscilloscope channel 1 the measurement
of the CM current through the victim channel signal cable and on channel 4 the generation of a DM interferece
voltage on the 50~$\Omega$ termination resistor.

\begin{figure}[t]
  \centerline{\includegraphics[width=.7\textwidth]{fig/cm-meas-second-chan}}
  \caption{\label{fig:cm-meas-second-chan} Common-mode current generating a differential-mode voltage}
\end{figure}

%==============================================================================
% SEC: Validation using SPICE model
%==============================================================================
\pagebreak
\section{Validation using SPICE model}
\label{sec:spice-mdl}

A model was built using LTspice~IV, in order to validate the cause of the
interference signal. This model is shown in Figure~\ref{fig:sim-model}. It simulates what the
signal at the end of a 1 meter long coaxial cable would be, which corresponds to our lab setup
for measuring the CM current generated at the offending channel.

First, the parasitic inter-winding capacitance is added to the model. Then, a simplified version
(containing only the inductors) of a multiconductor transmission line model~\cite{joffe_grounds_2010}
is used to model the cables through which the blocking signals are sent. An inductor is therefore placed
on each wire, which accounts for the following:

\begin{itemize}
  \item 0.45~$\mu$H accounting for the leakage inductance of the MSD1278 transformer~\cite{coilcraft_msd1278_2013}
  (0.9~$\mu$H in two equal parts on each pin of the secondary)
  \item 1.25~$\mu$H CM inductance per each wire, corresponding to the inductance
  of 1~m of copper cable
  \item 0.3~$\mu$H for the stray inductance of the traces from the blocking output stage
  to the cables
\end{itemize}

\noindent Finally, an inductor is placed after the 50~$\Omega$ termination resistor
to simulate a 3~m wire corresponding to the safety conductor (green wire) in the
220~V power line. The green wire is the return path of the CM current as shown in the
measurements in Section~\ref{sec:reason}.

The generation of a CM current through the parasitic inter-winding capacitance of the
transformer is shown in Figure~\ref{fig:sim-signal-generation}. It can be seen that the
CM current generated through the two capacitors is maximum when the slope of the voltage
drop on the capacitor is maximum, and zero when the slope of the voltage drop is zero.

Figure~\ref{fig:sim-offending-chan} shows the simulation waveforms at the output of the
offending channel, when its shield is connected to a victim channel. It can be seen
that the current (red) waveform is similar to that in Figure~\ref{fig:cm-meas-second-chan}.
Note also how the output voltage is affected by the half of the CM current (blue waveform)
generating a voltage through the 50~$\Omega$ termination.

Finally, the generation of the signal on the victim channel is shown in
Figure~\ref{fig:sim-victim-chan}. This is somewhat different from the measurement
in Figure~\ref{fig:cm-meas-second-chan}, in that not most of the CM current returns
through the shield of the victim channel. It can however be seen how some of the current
passes through the 50~$\Omega$ termination at the victim channel, generating a
DM voltage.

Note that some values in the simulation are relative, and not all details during the measurement
are taken into account. Details such as the exact cable inductance, cable resistance,
stray capacitance in the oscilloscope, etc., are unknowns and thus estimated.
This can in part explain the slight discrepancies between the measurements and the results
of running the SPICE model.

\begin{figure}
  \centerline{\includegraphics[width=\textwidth]{fig/sim-model}}
  \caption{\label{fig:sim-model} SPICE simulation model}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[width=\textwidth]{fig/sim-signal-generation}}
  \caption{\label{fig:sim-signal-generation} Common-mode current generation in the parasitic capacitance}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[width=\textwidth]{fig/sim-offending-chan}}
  \caption{\label{fig:sim-offending-chan} Simulation waveforms at offending channel}
\end{figure}

\begin{figure}
  \centerline{\includegraphics[width=\textwidth]{fig/sim-victim-chan}}
  \caption{\label{fig:sim-victim-chan} Simulation waveforms at victim channel}
\end{figure}

%==============================================================================
% SEC: Solution
%==============================================================================
\section{Solution}
\label{sec:solution}

Two solutions have been found to mitigate the problem:

\begin{itemize}
  \item connect the output connector shields to the CONV-TTL-BLO system's ground
  \item connect a 100~nF capacitor between the output connector shield and ground  
\end{itemize}

Another solution to avoid the problem would be to change the transformer for one
that has a lower inter-winding capacitance. This would however entail a re-design
of the front module board, which is not planned for now.

\subsection{Capacitor between shield and ground}

This solution is presented schematically in Figure~\ref{fig:sol-cap}, along with the
oscilloscope waveforms for the two outputs. As can be seen, a 100~nF capacitor placed
on the shield of the output connector precludes the generation of a DM voltage at the
victim channel by filtering a large part of the CM current to ground. Since most of the CM current
generating the interference voltage goes thorough the shield, removing this current
leads to effective removal of the interference voltage.

\begin{figure}
  \centerline{\includegraphics[width=.85\textwidth]{fig/sol-cap}}
  \caption{\label{fig:sol-cap} Capacitor between shield and ground, current shown when MOSFET turns off}
\end{figure}

When a capacitor is placed on the output shield in the SPICE simulation in
Section~\ref{sec:spice-mdl}, a resonance is generated as shown in Figure~\ref{fig:sim-resonance}.
This resonance is due to the inductor used to model the wires and may be the cause
of concern that a similar resonance might occur in reality.

It has been however checked in practice with different lengths of wires (0.5~m to $\sim$6~m)
that no oscillation appears as the wire length increases. Furthermore, as expected, the CM current gets
attenuated due to losses in the transmission line. Therefore, there seems to be no concern
of an oscillation appearing on the line.

Because placing a capacitor as shown in Figure~\ref{fig:sol-cap} removes the signal at a
victim channel, while keeping the isolation provided by the transformer. the capacitor
is the solution which will be implemented on the CONV-TTL-BLO system.

\begin{figure}
  \centerline{\includegraphics[width=\textwidth]{fig/sim-resonance}}
  \caption{\label{fig:sim-resonance} Simulated resonance when adding a capacitor on the shield}
\end{figure}

\subsection{Shield grounding}

Figure~\ref{fig:sol-gnd} shows schematically how this solution would be implemented.
The output signal is the same as with placing a capacitor to chassis ground (Figure~\ref{fig:sol-cap}).

Since the coaxial cables carrying the blocking signal may potentially go out of
a building and into another, this solution removes the isolation that the transformer
provides, rendering the transformer useless. In addition, loss of isolation at the output
stage may introduce potential ground loop problems~\cite{joffe_grounds_2010}, if the ground connections
of the transmitter and receiver ends in two different buildings are at different potentials, thus
possibly inferring further signal integrity problems.

\pagebreak
\begin{figure}[h]
  \centerline{\includegraphics[width=\textwidth]{fig/sol-gnd}}
  \caption{\label{fig:sol-gnd} Grounding the output connector shield}
\end{figure}

%==============================================================================
% SEC: Conclusion
%==============================================================================
\section{Conclusion}
\label{sec:conclusion}

Due to the parasitic capacitors inside the MSD1204 transformers used to galvanically
isolate the blocking output stage, an common-mode (CM) current is generated on the pulse
rising and falling edges, due to a change in voltage over the capacitors. When a second
CONV-TTL-BLO channel has its shield connected together with the pulse-generating
channel, this CM current finds a path through the second channel, and induces
a differential-mode (DM) interference voltage across the 50~$\Omega$ termination. This
voltage constitues what we were calling before the crosstalk signal.

The experiments carried out to describe how the CM current travels to close in loops
have been described in Section~\ref{sec:reason} and a SPICE model to validate
the generation of the signal is presented in Section~\ref{sec:spice-mdl}.

The best solution to mitigating this problem seems to be connecting a 100~nF capacitor
to the chassis ground from the conductor corresponding to the shield of the cable.
As has been shown in Section~\ref{sec:solution}, this safely solves the problem
at hand.

%==============================================================================
% APPENDICES
%==============================================================================
\pagebreak
\begin{appendices}

\section{Measuring transformer inter-winding capacitance using a digital multimeter}
\label{app:cap-meas}

This section deals with measuring the inter-winding capacitance of a transformer using
a digital multimeter. First, your multimeter needs to have a capacitance measurement
feature on the dial.

It is recommended to use short leads on the multimeter. Longer leads will introduce
extra capacitance, but this can be measured for an offset to our measurements. If you use
the normal long leads coming with the multimeter, the procedure is as follows:

\begin{enumerate}
  \item Set the multimeter to capacitance measurement mode
  \item Take the two probes in your hands and let the multimeter measurement settle
  \item Remember the value to which the probe settles; this will be your offset
  \item Put one probe on a pin on the primary side and the other probe to a pin on
  the secondary side
  \item Let the measurement settle; the measurement will be the total inter-winding
  capacitance, as seen in Figure~\ref{fig:cap-meas}
  \item Subtract the offset value from the measurement to obtain an estimate
  inter-winding capacitance for the transformer
\end{enumerate}

\begin{figure}[b]
  \centerline{\includegraphics[width=.81\textwidth]{fig/cap-meas}}
  \caption{\label{fig:cap-meas} Measuring inter-winding capacitance of a transformer}  
\end{figure}

\end{appendices}

%==============================================================================
% Bibliography
%==============================================================================
\pagebreak
\bibliographystyle{ieeetr}
\bibliography{crosstalk-report}

\end{document}
