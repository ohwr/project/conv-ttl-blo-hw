#!/usr/bin/python

import pylab as pl

ONE_USEC = 10**(-6)

x = []
y1 = []
y2 = []
y3 = []
y4 = []
y5 = []
y6 = []
y7 = []
ydiff = []

#################
# y1
# Blocking signal
#################
with open("outp.csv", 'r') as f:
    dat = f.read()
    dat = dat.replace(',',' ')
    dat = map(float, dat.split())
    for i in range(0, len(dat)):
        if (i%2):
            y1.append(dat[i])
        else:
            x.append(dat[i])

###########################################
# y2
# Oscilloscope input, CM crrt on both lines
###########################################
with open("cm-osc-in-both.csv", 'r') as f:
    dat = f.read()
    dat = dat.replace(',',' ')
    dat = map(float, dat.split())
    for i in range(0, len(dat)):
        if (i % 2):
            y2.append(dat[i])

############################################
# y3
# Oscilloscope output, CM crrt on both lines
############################################
with open("cm-osc-out-both.csv", 'r') as f:
    dat = f.read()
    dat = dat.replace(',',' ')
    dat = map(float, dat.split())
    for i in range(0, len(dat)):
        if (i % 2):
            y3.append(dat[i])

#############################################
# y4
# Oscilloscope output, current on signal line
#############################################
with open("cm-osc-out-p.csv", 'r') as f:
    dat = f.read()
    dat = dat.replace(',',' ')
    dat = map(float, dat.split())
    for i in range(0, len(dat)):
        if (i % 2):
            y4.append(dat[i])

############################################
# y5
# Oscilloscope output, current on shield line
############################################
with open("cm-osc-out-n.csv", 'r') as f:
    dat = f.read()
    dat = dat.replace(',',' ')
    dat = map(float, dat.split())
    for i in range(0, len(dat)):
        if (i % 2):
            y5.append(dat[i])

#################################
# y6
# Oscilloscope power line current
#################################
with open("cm-osc-out-pwr.csv", 'r') as f:
    dat = f.read()
    dat = dat.replace(',',' ')
    dat = map(float, dat.split())
    for i in range(0, len(dat)):
        if (i % 2):
            y6.append(dat[i])

#################################
# y7
# ELMA power line input current
#################################
with open("cm-elma-in-pwr.csv", 'r') as f:
    dat = f.read()
    dat = dat.replace(',',' ')
    dat = map(float, dat.split())
    for i in range(0, len(dat)):
        if (i % 2):
            y7.append(dat[i])

####################################
# x location for text
####################################
xloc = max(x) - 5*ONE_USEC

############################
# Add offsets & gain changes
############################
####################################################
# y1 - Blocking signal
# y2 - Oscilloscope input, CM crrt on both lines
# y3 - Oscilloscope output, CM crrt on both lines
# y4 - Oscilloscope output, current on signal line
# y5 - Oscilloscope output, current on shield line
# y6 - Oscilloscope power line current
# y7 - ELMA power line input current
####################################################

ydiff = y1[:]
ydiff2 = y2[:]

# DIFFERENCE
for i in range(len(y1)):
    ydiff[i] = y4[i] + y5[i] + y6[i] # y2[i] - (y4[i] + y5[i])
    ydiff2[i] = y2[i] - y3[i]
#    ydiff[i] = y6[i] - ydiff[i]
#    ydiff[i] = y7[i] - y6[i]
#    ydiff[i] = y7[i] - y6[i]

for i in range(len(ydiff)):
    ydiff[i] =  5 + 500*ydiff[i]
    ydiff2[i] = -5 + 500*ydiff2[i]

# blo -- 10V up
for i in range(len(y1)):
    y1[i] = 10+y1[i]

# osc. in line
# 20mV/div
for i in range(len(y2)):
    y2[i] = 30 + 500*y2[i]

# osc. out both lines
# 20mV/div
for i in range(len(y3)):
    y3[i] = 10 + 500*y3[i]

# osc. out signal line
# 20mV/div
for i in range(len(y4)):
    y4[i] = -10 + 500*y4[i]

# osc. out shield line
# 20mV/div
for i in range(len(y5)):
    y5[i] = -20 + 500*y5[i]

# osc. pwr line
# 20mV/div
for i in range(len(y6)):
    y6[i] = -30 + 500*y6[i]

# osc. pwr line
# 20mV/div
for i in range(len(y7)):
    y7[i] = -40 + 500*y7[i]


#pl.plot(x, y1, 'b')
pl.text(xloc, 32, "Osc. input", color='b', weight="bold")
pl.plot(x, y2, 'b')
pl.text(xloc, 12, "Osc. output, common-mode", color='r', weight="bold")
pl.plot(x, y3, 'r')
#pl.plot(x, ydiff, 'y')
#pl.plot(x, ydiff2, 'k')
pl.text(xloc, -8, "Osc. output, signal line", color='y', weight="bold")
pl.plot(x, y4, 'y')
pl.text(xloc, -18, "Osc. output, shield line", color='m', weight="bold")
pl.plot(x, y5, 'm')
pl.text(xloc, -28, "Osc. output, power line", color='c', weight="bold")
pl.plot(x, y6, 'c')
#pl.text(xloc, -23, "Crate input, power line", color='g', weight="bold")
#pl.plot(x, y7, 'g')

ax = pl.gca()
pl.setp(ax.get_xticklabels(), visible=False)
pl.setp(ax.get_yticklabels(), visible=False)
pl.xlabel("1 usec/div.")
pl.ylabel("20 mV/div.")

pl.axis([min(x), max(x), min(y1), max(y1)+1])
pl.xticks(pl.arange(min(x), max(x), ONE_USEC))
pl.yticks(pl.arange(-40, 50, 10))
pl.grid(True)

pl.savefig("plot.png")

#pl.show()

